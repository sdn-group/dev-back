from rest_framework import serializers
from .models import User

# https://www.fullstackpython.com/django-utils-encoding-smart-text-examples.html
from django.utils.encoding import smart_text
# https://github.com/py-bson/bson/blob/master/bson/objectid.py
from bson import ObjectId
from bson.errors import InvalidId
# https://www.django-rest-framework.org/api-guide/fields/#custom-fields Custom fields Serializer
# https://github.com/nesdis/djongo/issues/298 ObjectIdField


class ObjectIdField(serializers.Field):
    """ Serializer field for Djongo ObjectID fields """

    def to_internal_value(self, data):
        # Serialized value -> Database value
        try:
            # Get the ID, then build an ObjectID instance using it
            return ObjectId(str(data))
        except InvalidId:
            raise serializers.ValidationError(
                '`{}` is not a valid ObjectID'.format(data))

    def to_representation(self, value):
        # Database value -> Serialized value
        print('Printing Value to representation ...')
        print(value)
        # User submitted ID's might not be properly structured
        if not ObjectId.is_valid(value):
            raise InvalidId
        return smart_text(value)


class UserSerializer(serializers.ModelSerializer):
    _id = ObjectIdField(read_only=True)

    class Meta:
        model = User
        fields = ['_id', 'name', 'email', 'password']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        password = validated_data.pop("password", None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance
