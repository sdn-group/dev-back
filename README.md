<h1>Requirements</h1>

- Ubuntu 20.04
- OpenDayLigth Oxygen 0.8.3
- Mininet 2.3.0d5
- Python 3.8
- Django 3.0.5
- Pip 20.0.2
- Node 10.19.0
- Npm 6.14.4
- NuxtJS 2.15.3
- Next-UI 0.9.0
- MongoDB 4.4.7

<h1>OpenDayLight Oxygen Installation</h1>

We update our Ubuntu operating system

```
sudo apt-get -y update
sudo apt-get -y upgrade
```

Install java 8 development version for OpenDayLight Oxygen

```
sudo apt-get -y install openjdk-8-jre
```

Verify that your operating system is using java version 8.
 java version 8. Choose alternative **openjdk-8-jre** if another selection is found.

```
sudo update-alternatives --config java
```

Now we enter the environment variables to add the JAVA_PATH

```
sudo nano /etc/environment
```

We add the following line in new line at the end of what appears in the file.

```
JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre"
```

Finally, close the previous file and execute the following command to save changes.

```
source /etc/environment
```

Now we install **wget** to directly download the OpenDayLight driver.

```
sudo apt install wget
```

Download the OpenDayLight 0.8.3 tar.gz file extension by executing the following command.

```
wget https://nexus.opendaylight.org/content/repositories/opendaylight.release/org/opendaylight/integration/karaf/0.8.3/karaf-0.8.3.tar.gz
```

Now we create a directory where we will save this version of the SDN controller.

```
mkdir opendaylight
cd opendaylight
```

We execute the file copy command.

```
cp [path_to]/karaf-0.8.3.tar.gz karaf-0.8.3.tar.gz
```

Unzip the file karaf-0.8.3.tar.gz

```
tar -zxvf karaf-0.8.3.tar.gz
```

Enter the directory created and run karaf to start the controller.

```
cd karaf-0.8.3
sudo ./bin/karaf
```

Once the above is done, we should see a new console; which belongs to the SDN controller. In this console we must install the following features for the controller to allow access to its resources by an application.

```
feature:install odl-restconf odl-openflowplugin-flow-services odl-openflowplugin-drop-test odl-l2switch-switch odl-mdsal-apidocs odl-dlux-core odl-l2switch-switch-ui
```

It should not display any message if everything has been installed correctly. 
Every time you want to test the cbench tool with the OpenDayLight SDN driver you should run the following command in the driver console:

```
dropallpacketsrpc on
```

<h1>Mininet Installation</h1>

Return to the root folder or open a new console.

```
cd ~
```

We update the operating system packages.

```
sudo apt-get update -y
```

We install the version control software **git**.

```
sudo apt install git -y
```

We cloned the repository hosted on Github of the SDN-Mininet emulation software.

```
sudo git clone git://github.com/mininet/mininet
```

We enter the mininet folder.

```
cd mininet
```

The following command allows us to visualize the "versions" of the mininet.

```
git tag
```

Here we will use version 2.3.0d5 using the following command.

```
sudo git checkout -b 2.3.0d5
```

Exit the mininet folder and execute the following command to install the necessary files to start the mininet.

```
cd ..
sudo mininet/util/install.sh
```

Finally we test if everything is installed correctly using the following command.

```
sudo mn --test pingall
```

<h1>MongoDB Installation</h1>

NOTE: If _/etc/apt/sources.list.d/mongodb-org-4.4.list_ was installed incorrectly, we can delete it as follows:

```
sudo rm /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt-get update -y
```

The following commands will install MongoDB on Ubuntu: 
Common Errors: Common errors encountered may be due to typing 
NOTE: Check that double quotes are not curves, and check dashes.


```
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

sudo apt-get update -y

sudo apt-get install -y mongodb-org=4.4.7 mongodb-org-server=4.4.7 mongodb-org-shell=4.4.7 mongodb-org-mongos=4.4.7 mongodb-org-tools=4.4.7 

echo "mongodb-org hold" | sudo dpkg --set-selections

echo "mongodb-org-server hold" | sudo dpkg --set-selections

echo "mongodb-org-shell hold" | sudo dpkg --set-selections

echo "mongodb-org-mongos hold" | sudo dpkg --set-selections

echo "mongodb-org-tools hold" | sudo dpkg --set-selections

sudo systemctl start mongod

sudo systemctl status mongod

sudo systemctl enable mongod
```

<h1>Back-end Application Installation</h1>

In a new console we create a directory to store the back-end application written in Python 3.

```
mkdir back-end
cd back-end
```

The following command allows you to install all the Python packages needed to install the project. This command installs in particular the package for creating virtual environments, however, it also installs Python 3 in case it does not exist. Note: Python3 must refer to Python 3.8 or higher.

```
sudo apt-get install python3-venv -y
```

Now we create the virtual environment for the back-end application and activate the environment with the following command.

```
python3 -m venv env
source env/bin/activate
```

After the above we must notice that we are inside the environment if before the following console line appears "(env)".

Now we clone the back-end application from the public repository in GitLab

```
git clone https://gitlab.com/sdn-group/dev-back.git
```

We enter the back-end application directory and see that there is a requirements.txt file.

```
cd dev-back
ls
```

Run the following line to install all the necessary packages for the back-end application

```
python3 -m pip install -r requirements.txt
```

We run the user session migrations to avoid later warnings, although it is not necessary since the mongo database does not need migrations.

```
python3 manage.py migrate
```

Finally we run the integrated server of the Django framework.

```
python3 manage.py runserver 0.0.0.0:8000
```



