from auth import settings
from statistics.models import Statistics
from topologies.models import Topology
import requests
from datetime import datetime
import pytz


class OpenFlowStatistics:

    def __init__(self):
        pass  # Nothing to see here

    def get(self):
        """
        This function get statistics from OpenDayLight controller
        :return Statistics Object: Statistics object
        """
        response = requests.get(
            settings.ODL_RESTCONF_URL + '/opendaylight-inventory:nodes',
            auth=(settings.ODL_USERNAME, settings.ODL_PASSWORD))

        if response.status_code == 200:
            response_dict = response.json()
            if response_dict['nodes'] == {}:
                return False

            nodes_ = response_dict['nodes']['node']

            nodes = []
            for node_ in nodes_:
                node = {}

                node['dpid'] = node_['id']
                node['group_features'] = node_[
                    'opendaylight-group-statistics:group-features'] if 'opendaylight-group-statistics:group-features' in node_ else {}
                node['port_number'] = node_[
                    'flow-node-inventory:port-number'] if 'flow-node-inventory:port-number' in node_ else 0
                node['serial_number'] = node_[
                    'flow-node-inventory:serial-number'] if 'flow-node-inventory:serial-number' in node_ else ""
                node['hardware'] = node_[
                    'flow-node-inventory:hardware'] if 'flow-node-inventory:hardware' in node_ else ""
                node['description'] = node_[
                    'flow-node-inventory:description'] if 'flow-node-inventory:description' in node_ else ""
                node['software'] = node_[
                    'flow-node-inventory:software'] if 'flow-node-inventory:software' in node_ else ""
                node['switch_features'] = node_[
                    'flow-node-inventory:switch-features'] if 'flow-node-inventory:switch-features' in node_ else {}
                node['manufacturer'] = node_[
                    'flow-node-inventory:manufacturer'] if 'flow-node-inventory:manufacturer' in node_ else ""
                node['ip_address'] = node_[
                    'flow-node-inventory:ip-address'] if 'flow-node-inventory:ip-address' in node_ else ""
                node['snapshot_gathering_status_start'] = node_[
                    'flow-node-inventory:snapshot-gathering-status-start'] if 'flow-node-inventory:snapshot-gathering-status-start' in node_ else {}
                node['snapshot_gathering_status_end'] = node_[
                    'flow-node-inventory:snapshot-gathering-status-end'] if 'flow-node-inventory:snapshot-gathering-status-end' in node_ else {}

                tables = []
                for table_ in node_['flow-node-inventory:table']:
                    table = {}

                    table['id'] = table_['id']
                    table['flow_table_statistics'] = table_[
                        'opendaylight-flow-table-statistics:flow-table-statistics'] if 'opendaylight-flow-table-statistics:flow-table-statistics' in node_ else {}

                    flows = []
                    if 'flow' in table_:
                        for flow_ in table_['flow']:
                            flow = {}

                            flow['id'] = flow_['id']
                            flow['priority'] = flow_['priority']
                            flow['flow_statistics'] = flow_[
                                'opendaylight-flow-statistics:flow-statistics']
                            flow['table_id'] = flow_['table_id']
                            flow['cookie_mask'] = flow_['cookie_mask']
                            flow['hard_timeout'] = flow_['hard-timeout']
                            flow['match'] = flow_['match']
                            flow['cookie'] = flow_['cookie']
                            flow['flags'] = flow_['flags']
                            flow['instructions'] = flow_[
                                'instructions'] if 'instructions' in flow_ else {}
                            flow['idle_timeout'] = flow_['idle-timeout']

                            flows.append(flow)
                    table['flows'] = flows
                    tables.append(table)
                node['tables'] = tables

                ports = []
                for port_ in node_['node-connector']:
                    port = {}

                    port['dpid'] = port_['id']
                    port['port_number'] = port_[
                        'flow-node-inventory:port-number']
                    port['hardware_address'] = port_[
                        'flow-node-inventory:hardware-address']
                    port['current_feature'] = port_[
                        'flow-node-inventory:current-feature']
                    port['current_speed'] = port_[
                        'flow-node-inventory:current-speed'] if 'flow-node-inventory:current-speed' in port_ else 0
                    port['maximun_speed'] = port_[
                        'flow-node-inventory:maximum-speed'] if 'flow-node-inventory:maximum-speed' in port_ else 0
                    port['name'] = port_[
                        'flow-node-inventory:name'] if 'flow-node-inventory:name' in port_ else ""
                    port['state'] = port_[
                        'flow-node-inventory:state'] if 'flow-node-inventory:state' in port_ else {}
                    port['flow_capable_node_connector_statistics'] = port_[
                        'opendaylight-port-statistics:flow-capable-node-connector-statistics'] if 'opendaylight-port-statistics:flow-capable-node-connector-statistics' in port_ else {}

                    host_trackers = []
                    if 'address-tracker:addresses' in port_:
                        for address_tracker in port_['address-tracker:addresses']:
                            host_tracker = {}

                            host_tracker['id'] = address_tracker['id']
                            host_tracker['mac'] = address_tracker['mac']
                            # Dividing 1000.0 in order to convert in seconds(s) from ms
                            host_tracker['first_seen'] = datetime.fromtimestamp(
                                address_tracker['first-seen'] / 1000.0, tz=pytz.UTC)
                            host_tracker['last_seen'] = datetime.fromtimestamp(
                                address_tracker['last-seen'] / 1000.0, tz=pytz.UTC)
                            host_tracker['ip'] = address_tracker['ip']

                            host_trackers.append(host_tracker)
                    port['addresses_tracker'] = host_trackers
                    ports.append(port)
                node['ports'] = ports

                nodes.append(node)

            # Getting recorded topologies
            latest_recorded_topology = Topology.objects.filter(
                is_recorded__exact=True)
            # if not recorded topologies
            if not latest_recorded_topology:
                print('Not recorded Topology to save')
                return False
            # Getting latest Recorded Topology
            # https://docs.djangoproject.com/en/dev/ref/models/querysets/#latest
            latest_recorded_topology = latest_recorded_topology.latest(
                'created_at')

            statistics = Statistics(
                nodes=nodes,
                topology=latest_recorded_topology
            )
            return statistics
        return False
