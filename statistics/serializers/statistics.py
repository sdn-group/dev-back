from rest_framework import serializers
from auth.custom.fields.serializers import ObjectIdField

from statistics.models import Statistics


class AddressTrackerSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    mac = serializers.CharField(max_length=200)
    first_seen = serializers.DateTimeField()
    last_seen = serializers.DateTimeField()
    ip = serializers.IPAddressField()

    class Meta:
        fields = '__all__'


class PortSerializer(serializers.Serializer):
    dpid = serializers.CharField(max_length=255)
    port_number = serializers.IntegerField()
    hardware_address = serializers.CharField(max_length=200)
    current_feature = serializers.CharField(max_length=200)
    current_speed = serializers.IntegerField()
    maximun_speed = serializers.IntegerField()
    name = serializers.CharField()
    state = serializers.JSONField()
    flow_capable_node_connector_statistics = serializers.JSONField()
    addresses_tracker = serializers.SerializerMethodField()

    class Meta:
        fields = '__all__'

    def get_addresses_tracker(self, obj):
        addresses_tracker_serializer = AddressTrackerSerializer(
            obj['addresses_tracker'], many=True)
        return addresses_tracker_serializer.data


class NodeSerializer(serializers.Serializer):
    dpid = serializers.CharField(max_length=255)
    group_features = serializers.JSONField()
    ports = serializers.SerializerMethodField()
    tables = serializers.JSONField()
    port_number = serializers.IntegerField()
    serial_number = serializers.CharField(max_length=200)
    hardware = serializers.CharField(max_length=200)
    description = serializers.CharField()
    software = serializers.CharField(max_length=200)
    switch_features = serializers.JSONField()
    manufacturer = serializers.CharField(max_length=200)
    ip_address = serializers.IPAddressField()
    snapshot_gathering_status_start = serializers.JSONField()
    snapshot_gathering_status_end = serializers.JSONField()

    class Meta:
        fields = '__all__'

    def get_ports(self, obj):
        ports_serializer = PortSerializer(obj['ports'], many=True)
        return ports_serializer.data


class StatisticsSerializer(serializers.ModelSerializer):
    _id = ObjectIdField(read_only=True)

    # Nested Objects
    # https://github.com/nesdis/djongo/issues/73
    nodes = serializers.SerializerMethodField()

    class Meta:
        model = Statistics
        fields = ['_id', 'nodes', 'created_at']

    def get_nodes(self, obj):
        nodes_serializer = NodeSerializer(obj.nodes, many=True)
        return nodes_serializer.data
