from topologies.models import Topology
from djongo import models
from django import forms

class AddressTracker(models.Model):
    id          = models.IntegerField()
    mac         = models.CharField(max_length=200)
    first_seen  = models.DateTimeField()
    last_seen   = models.DateTimeField()
    ip          = models.GenericIPAddressField()
    class Meta:
        abstract = True

class Port(models.Model):
    dpid            = models.CharField(max_length=255)
    port_number     = models.IntegerField()
    hardware_address= models.CharField(max_length=200)
    current_feature = models.CharField(max_length=200)
    current_speed   = models.IntegerField()
    maximun_speed   = models.IntegerField()
    name            = models.TextField()
    state           = models.JSONField()
    flow_capable_node_connector_statistics = models.JSONField()
    addresses_tracker   = models.ArrayField(
        model_container = AddressTracker
    )
    class Meta:
        abstract = True

class Flow(models.Model):
    id              = models.CharField(max_length=255)
    priority        = models.IntegerField()
    flow_statistics  = models.JSONField()
    table_id        = models.IntegerField()
    cookie_mask     = models.IntegerField()
    hard_timeout    = models.IntegerField()
    match           = models.JSONField()
    cookie          = models.IntegerField()
    flags           = models.TextField()
    instructions    = models.JSONField()
    idle_timeout    = models.IntegerField()
    class Meta:
        abstract = True

class Table(models.Model):
    id                      = models.IntegerField()
    flow_table_statistics   = models.JSONField()
    flows                   = models.ArrayField(
        model_container = Flow
    )
    class Meta:
        abstract = True

class Node(models.Model):
    dpid            = models.CharField(max_length=255)
    ports           = models.ArrayField(
        model_container = Port
    )
    group_features  = models.JSONField()
    port_number     = models.IntegerField()
    serial_number   = models.CharField(max_length=200)
    tables          = models.ArrayField(
        model_container = Table
    )
    hardware        = models.CharField(max_length=200)
    description     = models.TextField()
    software        = models.CharField(max_length=200)
    switch_features = models.JSONField()
    manufacturer    = models.CharField(max_length=200)
    ip_address      = models.GenericIPAddressField()
    snapshot_gathering_status_start = models.JSONField()
    snapshot_gathering_status_end   = models.JSONField()
    class Meta:
        abstract = True

class Statistics(models.Model):
    _id             = models.ObjectIdField()
    nodes           = models.ArrayField(
        model_container = Node
    )
    topology = models.ForeignKey(
        Topology,
        on_delete=models.CASCADE,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    objects     = models.DjongoManager()

class StatisticsForm(forms.ModelForm):
    class Meta:
        model = Statistics
        fields = (
            '_id',
            'nodes',
            'topology',
        )