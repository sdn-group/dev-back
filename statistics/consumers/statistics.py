from djangochannelsrestframework.generics import GenericAsyncAPIConsumer
from djangochannelsrestframework.mixins import ListModelMixin
from djangochannelsrestframework.decorators import action
from djangochannelsrestframework.observer import model_observer

from statistics.models import Statistics
from statistics.serializers.statistics import StatisticsSerializer


class StatisticsConsumer(
        GenericAsyncAPIConsumer):
    queryset = Statistics.objects.all()
    serializer_class = StatisticsSerializer

    @model_observer(Statistics)
    async def message_activity(self, message, observer=None, action=None, **kwargs):
        # send activity to your frontend
        print('Sending statistics activity')
        await self.send_json(message)

    @message_activity.serializer
    def message_activity(self, instance: Statistics, action, **kwargs):
        '''This will return the message serializer'''
        print('Printing a serialized statistics')
        return dict(data=StatisticsSerializer(instance).data, action=action.value)

    @action()
    async def subscribe_to_statistics_activity(self, **kwargs):
        # check user has permission to do this
        print('subscribing to statistics activity')
        await self.message_activity.subscribe()
