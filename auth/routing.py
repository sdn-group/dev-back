from topologies.consumers.message import MessageConsumer
from django.core.asgi import get_asgi_application
from django.urls import path  # new

from channels.routing import ProtocolTypeRouter, URLRouter  # changed

from topologies.consumers.message import MessageConsumer
from statistics.consumers.statistics import StatisticsConsumer

application = ProtocolTypeRouter({
    'http': get_asgi_application(),
    # new
    'websocket': URLRouter([
        path('ws/messages', MessageConsumer.as_asgi()),
        path('ws/statistics', StatisticsConsumer.as_asgi()),
    ]),
})
