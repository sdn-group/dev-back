from rest_framework import serializers
from django.utils.encoding import smart_text # https://www.fullstackpython.com/django-utils-encoding-smart-text-examples.html
# https://github.com/py-bson/bson/blob/master/bson/objectid.py
from bson import ObjectId
from bson.errors import InvalidId
# https://www.django-rest-framework.org/api-guide/fields/#custom-fields Custom fields Serializer
# https://github.com/nesdis/djongo/issues/298 ObjectIdField
class ObjectIdField(serializers.Field):
    """ Serializer field for Djongo ObjectID fields """
    def to_internal_value(self, data):
        # Serialized value -> Database value
        try:
            return ObjectId(str(data))  # Get the ID, then build an ObjectID instance using it
        except InvalidId:
            raise serializers.ValidationError(
                '`{}` is not a valid ObjectID'.format(data))

    def to_representation(self, value):
        # Database value -> Serialized value
        if not ObjectId.is_valid(value):  # User submitted ID's might not be properly structured
            raise InvalidId
        return smart_text(value)