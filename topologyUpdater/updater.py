from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from topologyUpdater import topologyApi

def start():
    # First Topology GET
    topologyApi.update_topology()
    # Creating Topology GET job
    scheduler = BackgroundScheduler()
    scheduler.add_job(topologyApi.update_topology, 'interval', seconds=5)
    scheduler.start()