from statistics.classes.openflow_statistics import OpenFlowStatistics
import requests
from topologies.models import Topology, Message
from django.conf import settings
from django.forms.models import model_to_dict

class TopologyClass:
    def get():
        topology = Topology()
        # https://docs.python-requests.org/en/master/user/authentication/#basic-authentication
        try:
            odl_topology = requests.get(settings.ODL_RESTCONF_URL + '/network-topology:network-topology/topology/flow:1', auth=('admin', 'admin'))
        except:
            return False
        if odl_topology.status_code == 200:  # SUCCESS
            odl_topology_json = odl_topology.json()
            if odl_topology_json == {}:
                return False
            topology.topology_id = odl_topology_json['topology'][0]['topology-id']
            nodes = []
            if 'node' in odl_topology_json['topology'][0]:
                for odlNode in odl_topology_json['topology'][0]['node']:
                    node = {}
                    node['dpid'] = odlNode['node-id']
                    node['name'] = odlNode['node-id']
                    ports = []
                    if 'termination-point' in odlNode:
                        for odlPort in odlNode['termination-point']:
                            port = {}
                            port['dpid'] = odlNode['node-id']
                            port['tpid'] = odlPort['tp-id']
                            port['is_down'] = 0
                            ports.append(port)
                    node['ports'] = ports
                    node['is_down'] = 0
                    nodes.append(node)
            topology.nodes = nodes
            links = []
            if 'link' in odl_topology_json['topology'][0]:
                for odlLink in odl_topology_json['topology'][0]['link']:
                    # ref https://github.com/martimy/flowmanager/blob/master/js/topology.js // prevent duplicate links
                    if ( odlLink['source']['source-node'] < odlLink['destination']['dest-node'] ):
                        link = {}
                        link['dpid'] = odlLink['link-id']
                        link['source'] = odlLink['source']['source-node']
                        link['destination'] = odlLink['destination']['dest-node']
                        link['is_down'] = 0
                        links.append(link)
            topology.links = links
        return topology
    def compare(topology_to_compare):
        # Getting recorded topologies
        latest_recorded_topology = Topology.objects.filter(is_recorded__exact=True)
        # if not recorded topologies 
        if not latest_recorded_topology:
            print('Not recorded Topology to compare')
            return topology_to_compare, [], []
        # Getting latest Recorded Topology
        # https://docs.djangoproject.com/en/dev/ref/models/querysets/#latest
        latest_recorded_topology = latest_recorded_topology.latest('created_at')
        # The Django Model to a dictonary
        dict_latest_recorded_topology = model_to_dict( latest_recorded_topology )
        dict_topology_to_compare = model_to_dict( topology_to_compare )
        # Copying a compared Topology from a topology to compare
        topology_compared = topology_to_compare
        # Loop for comparing nodes
        ## Loop in lastest recorded topology nodes
        is_down_nodes = dict_latest_recorded_topology['nodes'].copy()
        for node_to_compare in dict_topology_to_compare['nodes']:
            ### Loop in the incoming topology nodes
            for node in is_down_nodes:
                if node_to_compare['dpid'] == node['dpid']:
                    is_down_nodes.remove(node)
                    break
                pass
            pass
        ## Loop for changing is_down atribute to remaining nodes
        for node in is_down_nodes:
            node['is_down'] = 1
            ### Adding is_down nodes to incoming topology nodes
            topology_compared.nodes.append(node)
            pass
        ## Loop in lastest recorded topology links
        is_down_links = dict_latest_recorded_topology['links'].copy()
        for link_to_compare in dict_topology_to_compare['links']:
            ### Loop in the incoming topology links
            for link in is_down_links:
                if link_to_compare['dpid'] == link['dpid']:
                    is_down_links.remove(link)
                    break
                pass
            pass
        ## Loop for changing is_down atribute to remaining links
        for link in is_down_links:
            link['is_down'] = 1
            ### Adding is_down nodes to incoming topology nodes
            topology_compared.links.append(link)
            pass
        # topology_compared is a Topology Model
        # is_down_nodes is a nodes down list
        # is_down_links is a links down list
        return topology_compared, is_down_nodes, is_down_links

class MessageClass:
    def save(nodes, links, recorded_topology = None):
        try:
            # Getting recorded topologies
            recorded_topologies = Topology.objects.filter(is_recorded__exact=True)
            # If not lastest recorded topology
            if not recorded_topologies:
                print('Not recorded Topology')
                return
            # Getting latest recorded topology
            latest_recorded_topology = recorded_topologies.latest('created_at')
            message = None
            message_all = Message.objects.filter(topology__exact=latest_recorded_topology)
            nodes_message = list( message_all.filter(_object_type__exact='node') )
            links_message = list( message_all.filter(_object_type__exact='link') )
            for node in nodes:
                duplicate = False
                for _node in nodes_message:
                    if node['dpid'] == _node._object_id:
                        nodes_message.remove(_node)
                        duplicate = True
                        break
                if duplicate:
                    continue
                message = Message(
                    text='Node ' + node['dpid'] + ' has been found in down mode', 
                    topology=latest_recorded_topology,
                    _object_type='node',
                    _object_id=node['dpid']
                    )
                message.save()
            message = None
            for link in links:
                duplicate = False
                for _link in links_message:
                    if link['dpid'] == _link._object_id:
                        links_message.remove(_link)
                        duplicate = True
                        break
                if duplicate:
                    continue
                message = Message(
                    text='Link Source: ' + link['source'] + ' and Destination: ' + link['destination'] + ' has been found in down mode', 
                    topology=latest_recorded_topology,
                    _object_type='link',
                    _object_id=link['dpid']
                    )
                message.save()
            return
        except ValueError:
            raise ValueError


def update_topology():
    topology = TopologyClass.get()
    if not topology:
        return
    try:
        compared_topology, nodes_down, links_down = TopologyClass.compare(topology)
        compared_topology.save()
        MessageClass.save(nodes_down, links_down)
        p = OpenFlowStatistics()
        statistics = p.get()
        # if something wrong then statistics false
        if statistics:
            statistics.save()
        else:
            return
        print("Saving...\n", end = "\r", flush=True)
    except:
        print('Getting Error', end = "\r", flush=True)
        raise
