from topologies.models import Topology
from topologies.serializer import MessageSerializer
from topologyUpdater.topologyApi import TopologyClass
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from django.forms.models import model_to_dict
import jwt
from rest_framework import status

# Create your views here.
class TopologyView(APIView):
    def get(self, request):
        token = request.COOKIES.get('jwt')
        if not token:
            raise AuthenticationFailed('Unauthenticated!')  
        try:
            jwt.decode(token, 'secret', algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        topology = Topology.objects.latest('created_at')
        dict_topology = model_to_dict( topology )
        dict_topology['_id'] = str(dict_topology['_id'])
        return Response(dict_topology, status.HTTP_200_OK)

    def post(self, request):
        token = request.COOKIES.get('jwt')
        if not token:
            raise AuthenticationFailed('Unauthenticated!')  
        try:
            jwt.decode(token, 'secret', algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        topology = TopologyClass.get()
        if not topology:
            return Response({}, status.HTTP_503_SERVICE_UNAVAILABLE)
        topology.is_recorded = 1
        topology.save()
        return Response({}, status.HTTP_201_CREATED)

class MessageView(APIView):
    def get(self, request):
        # Verifying Auth
        token = request.COOKIES.get('jwt')
        if not token:
            raise AuthenticationFailed('Unauthenticated!')  
        try:
            jwt.decode(token, 'secret', algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')
        # Getting recorded topologies
        latest_recorded_topology = Topology.objects.filter(is_recorded__exact=True)
        # If not lastest recorded topology
        if not latest_recorded_topology:
            print('Not recorded Topology')
            return Response({}, status.HTTP_302_FOUND)
        # Getting latest recorded topology
        latest_recorded_topology = latest_recorded_topology.latest('created_at')
        # ---
        list_ = []
        for item in latest_recorded_topology.message_set.all().order_by('-created_at').values():
            list_.append(MessageSerializer(item).data)
        # Return a list of messages from latest recorded topology
        print(len(list_))
        return Response(list_, status.HTTP_200_OK)