from auth.custom.fields.serializers import ObjectIdField
from rest_framework import serializers
from .models import Message, Topology


class TopologySerializer(serializers.ModelSerializer):
    class Meta:
        model = Topology
        fields = ['id', 'topology_id', 'nodes', 'links', 'created_at']


class MessageSerializer(serializers.ModelSerializer):
    _id = ObjectIdField(read_only=True)

    class Meta:
        model = Message
        fields = ['_id', 'text', 'created_at']
