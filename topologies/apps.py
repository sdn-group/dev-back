from django.apps import AppConfig


class TopologiesConfig(AppConfig):
    name = 'topologies'

    def ready(self):
        from topologyUpdater import updater
        updater.start()