from djangochannelsrestframework.generics import GenericAsyncAPIConsumer
from djangochannelsrestframework.decorators import action
from djangochannelsrestframework.observer import model_observer

from topologies.models import Message
from topologies.serializer import MessageSerializer


class MessageConsumer(
    GenericAsyncAPIConsumer
):

    @model_observer(Message)
    async def message_activity(self, message, observer=None, action=None, **kwargs):
        # send activity to your frontend
        print('Sending Message Activity')
        await self.send_json(message)

    @message_activity.serializer
    def message_activity(self, instance: Message, action, **kwargs):
        '''This will return the message serializer'''
        print('Printing a serialized Message')
        return dict(data=MessageSerializer(instance).data, action=action.value)

    @action()
    async def subscribe_to_message_activity(self, **kwargs):
        # check user has permission to do this
        print('subscribing to message activity')
        await self.message_activity.subscribe()
