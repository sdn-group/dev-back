from django import forms
from djongo import models

# Create your models here.
class Port(models.Model):
    dpid    = models.CharField(max_length=200)  # Datapath id = Openflow:1
    tpid    = models.CharField(max_length=200)  # Termination id = Openflow:1:1
    is_down = models.BooleanField(default=False)    # Is it down? = True=1 / False=0
    class Meta:
        abstract = True

class PortForm(forms.ModelForm):
    class Meta:
        model = Port
        fields = (
            'dpid',
            'tpid',
            'is_down'
        )

class Link(models.Model):
    dpid    = models.CharField(max_length=200)          # Datapath id = Link Id, e.g Openflow:5:2
    source  = models.CharField(max_length=200)          # Source = sour
    destination  = models.CharField(max_length=200)
    is_down = models.BooleanField(default=False)
    class Meta:
        abstract = True

class LinkForm(forms.ModelForm):
    class Meta:
        model = Link
        fields = (
            'dpid',
            'source',
            'destination',
            'is_down'
        )

class Node(models.Model):
    dpid    = models.CharField(max_length=200)
    name    = models.CharField(max_length=200)
    ports   = models.ArrayField(
        model_container  = Port,
        model_form_class = PortForm
    )
    is_down = models.BooleanField(default=False)
    class Meta:
        abstract = True

class NodeForm(forms.ModelForm):
    class Meta:
        model = Node
        fields = (
            'dpid',
            'name',
            'ports',
            'is_down'
        )

class Topology(models.Model):
    _id = models.ObjectIdField()
    topology_id = models.CharField(max_length=200)
    nodes       = models.ArrayField(
        model_container  = Node,
        model_form_class = NodeForm
    )
    links       = models.ArrayField(
        model_container  = Link,
        model_form_class = LinkForm
    )
    is_recorded = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects     = models.DjongoManager()

class TopologyForm(forms.ModelForm):
    class Meta:
        model = Topology
        fields = (
            '_id',
            'topology_id',
            'nodes',
            'links',
            'is_recorded'
        )

# ObjectIDField for a recall
# REFERENCE: https://www.djongomapper.com/using-django-with-other-fields/
class Message(models.Model):
    _id = models.ObjectIdField()
    text = models.TextField()
    topology = models.ForeignKey(
        Topology,
        on_delete=models.CASCADE,
    )
    _object_type = models.CharField(max_length=200)
    _object_id = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = (
            '_id',
            'text',
            'topology',
            '_object_type',
            '_object_id'
        )