from topologies.views import MessageView, TopologyView
from django.urls import path

urlpatterns = [
    path('topologies', TopologyView.as_view()),
    path('messages', MessageView.as_view())
]